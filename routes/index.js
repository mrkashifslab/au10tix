const express = require('express')
const documentController = require('../controllers/document.controller')

const router = express.Router()
const { catchErrors } = require('../handlers/error.handler')

router.get('/ping', (req, res) => {
  res.json({
    message: 'pong'
  })
})

router.post(
  '/api/v1',
  catchErrors(documentController.handleFormUpload),
  catchErrors(documentController.handleForm)
)

router.post(
  '/api/v1/monkey',
  documentController.validateRequest,
  documentController.handleFilesinJSON,
  documentController.handleFilesError,
  documentController.handleForm
)

router.post(
  '/api/v1/monkey/report',
  documentController.validateRequest,
  catchErrors(documentController.provideReport),
  catchErrors(documentController.handleFilesinJSON),
  catchErrors(documentController.handleForm)
)

module.exports = router
