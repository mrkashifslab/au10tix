const { catchErrors } = require('./error.handler')

describe('Function to handle the errors', () => {
  test('It should throw an error', () => {
    expect(catchErrors('next')).toThrow()
  })
})
