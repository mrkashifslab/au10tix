;(async () => {
  try {
    console.info('Au10tix MS → setting environment')

    require('./config/initializers/environment')()

    const nconf = require('nconf')

    console.info('Au10tix MS → starting initialization')

    await require('./config/initializers/directories')(
      Object.values(nconf.get('dirs')).map(dir => {
        return { dir }
      })
    )

    await require('./config/initializers/database')(nconf.get('database:url'), {
      useNewUrlParser: true,
      useCreateIndex: true
    })

    await require('./config/initializers/server')(nconf.get('port'))

    console.info(`Au10tix MS → initialized successfully`)

    return true
  } catch (err) {
    console.error(`Au10tix MS → failed to initialize`, err)

    throw err
  }
})()
