const validateForm = require('./validate.helper')

describe('Validation helper spec', () => {
  test('Return true if schema exists and payload is correct', () => {
    expect(
      validateForm(
        {
          frontSide: '/abc/test.js',
          docketId: 123,
          elementalClient: {
            id: 4,
            name: 'Fazremit',
            apiKey: 'fc33d26e',
            secretKey: 'ff5e9fc6'
          }
        },
        'document.form'
      )
    ).toBe(true)
  })

  test('Throw an error if the schema does not exist', () => {
    expect(() =>
      validateForm(
        {
          frontSide: '/abc/test.js',
          docketId: 123
        },
        'document.test.a'
      )
    ).toThrow()
  })
})
