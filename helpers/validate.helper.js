const validationUtil = require('../util/ajv.util')
const schemas = require('../config/validations.json')

// Validate the payload against its schema
module.exports = (data, schema) => {
  const nestedSchema = schema.split('.')

  try {
    schema = schemas[nestedSchema[0]]

    nestedSchema.forEach((sch, i) => {
      if (!i) return
      schema = schema[sch]
    })
  } catch (err) {
    throw new Error('Schema not found')
  }

  return validationUtil(data, schema)
}
