const MockServer = require('../test/helpers/mockServer.helper')
require('../config/initializers/environment')()
const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const connectDatabase = require('../config/initializers/database')
const mockPayload = require('../test/assets/payload.js').mockPayload

const mockRequest = body => ({
  body
})

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res)
  res.json = jest.fn().mockReturnValue(res)
  return res
}

describe('Mocks the Document controller', () => {
  const mockServer = new MockServer(8099)
  const mongoServer = new MongoMemoryServer()

  beforeAll(async () => {
    // require('../config/initializers/environment')()
    const mongoUri = await mongoServer.getConnectionString()
    await connectDatabase(mongoUri, {
      useNewUrlParser: true
    })
  })
  afterAll(async () => {
    await mongoServer.stop()
    await mockServer.stop()
  })

  afterEach(async () => {
    await mongoose.disconnect()
  })

  test('It should handle FormUpload and should not throw error', async () => {
    const documentController = require('./document.controller')
    const req = mockRequest({
      docketId: '54452',
      fontSide: 'test/assets/temp/passport'
    })
    const res = mockResponse()

    await expect(() => documentController.handleForm(req, res)).not.toThrow()
  })

  test('It should Provide Report and should not to throw error', async () => {
    const port = mockServer.port
    const documentController = require('./document.controller')

    const req = mockRequest(mockPayload(port))
    const res = mockResponse()

    try {
      await documentController.provideReport(req, res)

      expect(res.json).toHaveBeenCalledWith({
        message: 'Report posted to elemental'
      })
    } catch (error) {}
  })

  test('It should get the file from the payload object', async () => {
    const port = mockServer.port
    const documentController = require('./document.controller')

    const req = mockRequest(mockPayload(port))
    const res = mockResponse()

    try {
      await documentController.provideReport(req, res)
      expect(res.status).toHaveBeenCalledWith(200)
    } catch (error) {}
  })

  test('It should the values in payload object', async () => {
    const port = mockServer.port
    const documentController = require('./document.controller')

    const req = mockRequest(mockPayload(port))
    const res = mockResponse()

    try {
      await documentController.validateRequest(req, res)
      expect(res.status).not.toBe(400)
    } catch (error) {
      console.log(error)
    }
  })

  test('It should process the data passed in payload object > handleFilesInJSON', async () => {
    const port = mockServer.port
    const documentController = require('./document.controller')

    const req = mockRequest(mockPayload(port))
    const res = mockResponse()

    try {
      await documentController.handleFilesinJSON(req, res)
      expect(res.status).toHaveBeenCalledWith(200)
    } catch (error) {}
  })

  // test('It should handle the Form upload', async () => {
  //   const documentController = require('./document.controller')

  //   const res = mockResponse()
  //   const req = mockRequest({
  //     frontSide: 'test/assets/temp/passport'
  //   })

  //   try {
  //     await documentController.handleFormUpload(req, res)
  //     expect(res.status).not.toBe(400)
  //   } catch (error) {
  //     console.log(error)
  //   }
  // })
})
