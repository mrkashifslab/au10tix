const fs = require('fs')
const util = require('util')
const nconf = require('nconf')
const mongoose = require('mongoose')
const multer = require('multer')
const uuidv4 = require('uuid/v4')
const _ = require('../util/fns.util')
const monkeyHelper = require('../services/monkey.service')
const documentService = require('../services/document.service')
const au10tixService = require('../services/au10tix.service')
const elementalService = require('../services/elemental.service')
const validateForm = require('../helpers/validate.helper')
const IncomingRequestModel = mongoose.model('IncomingRequest')
const DocumentModel = mongoose.model('Document')
const generatePdf = require('../services/document.service/generatePDFReport')
//  eslint-disable-next-line
const exists = util.promisify(fs.exists)

const handleFormUpload = async (req, res, next) => {
  class ValidationError extends Error {
    constructor(message, code) {
      super(message)
      this.code = code
      this.name = 'MulterValidationError'
    }
  }

  // max filesize that can be uploaded
  const mb = 3.9
  let kb, by
  kb = by = 1024
  const maxFileSize = mb * kb * by
  const maxFilesAllowed = 2
  const supportedFileTypes = ['jpeg', 'tiff', 'pdf']

  // define multer options for form upload
  const multerOptions = {
    preservePath: true,
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, nconf.get('dirs:uploads'))
      },
      filename: (req, file, cb) => {
        cb(
          null,
          uuidv4() + '-' + Date.now() + '.' + file.mimetype.split('/')[1]
        )
      }
    }),
    limits: {
      fileSize: maxFileSize,
      files: maxFilesAllowed
    },
    fileFilter: (req, file, cb) => {
      const type = file.mimetype.split('/')[1]
      if (supportedFileTypes.includes(type)) {
        cb(null, true)
      } else {
        cb(
          new ValidationError('Filetype is not supported', 'FILE_FILTER'),
          false
        )
      }
    }
  }

  const upload = multer(multerOptions).fields([
    {
      name: 'frontSide',
      maxCount: 1
    },
    {
      name: 'backSide',
      maxCount: 1
    },
    {
      name: 'photo',
      maxCount: 1
    }
  ])

  upload(req, res, err => {
    if (err instanceof multer.MulterError) {
      console.log('multer error -', err)
      // A Multer error occurred when uploading.
      const { message, code, field } = err

      switch (code) {
        case 'LIMIT_FILE_SIZE':
          res.status(400).json({
            status: false,
            message,
            field,
            maxFileSize: `${mb} mb`
          })
          break

        case 'LIMIT_FILE_COUNT':
          res.status(400).json({
            status: false,
            message
          })
          break

        default:
          break
      }

      return false
    } else if (err) {
      // An unknown error occurred when uploading.
      if (err.code === 'FILE_FILTER') {
        res.status(400).json({
          status: false,
          message: err.message,
          supported: supportedFileTypes
        })
      }
      return false
    }

    if (Object.keys(req.files).length === 0 || !req.body.docketId) {
      return res.status(400).json({
        status: false,
        message: 'Some fields are missing!'
      })
    } else {
      // Populate body with file location
      for (const key in req.files) {
        req.body[key] = req.files[key][0].path
      }
    }

    // Everything went fine.
    next()
  })
}

const validateRequest = (req, res, next) => {
  try {
    validateForm(req.body, 'document.form')
    next()
  } catch (err) {
    if (!res.headersSent) {
      return res.status(400).json({
        status: false,
        message: err.message || 'Something went wrong, try again.'
      })
    }
  }
}

const handleForm = async (req, res) => {
  try {
    let incomingReq = await IncomingRequestModel(req.body).save()
    incomingReq = incomingReq._doc
    res.json({
      status: true,
      id: incomingReq._id
    })

    documentService
      .processDoc(incomingReq, {
        uploadPath: nconf.get('dirs:uploads'),
        pdfPath: nconf.get('dirs:pdfs'),
        reportPath: nconf.get('reportPath'),
        sendTranscription: incomingReq.autoTranscribe
      })
      .catch(console.error)

    return
  } catch (err) {
    if (!res.headersSent) {
      return res.status(400).json({
        status: false,
        message: err.message || 'something went wrong, try again.'
      })
    }
  }
}

const handleFilesinJSON = async (req, res, next) => {
  try {
    req.body = await monkeyHelper.getFiles(req.body, nconf.get('dirs:uploads'))
    res.status(200)
    next()
  } catch (err) {
    next(err)
    // if (!res.headersSent) {
    //   res.status(400).json({
    //     status: false,
    //     error: err
    //   })
    // }
  }
}

// Sending Error report to Elemental for file too large
const handleFilesError = async (err, req, res, next) => {
  if (err) {
    res.json({
      success: false,
      message: err.message
    })

    if (err.type === 'FileTooLarge' || err.type === 'FileNotSupported') {
      const message = req.body
      const pdfError = {
        authentic: false,
        docketId: message.docketId,
        documentData: null,
        documentQuality: null,
        documentTests: null,
        error: {
          message: 'Rejected request, reason - File Not Supported'
        }
      }

      await generatePdf(pdfError, '', nconf.get('dirs:pdfs'))

      const data = {
        authentic: false,
        docketId: message.docketId,
        frontSideId: message.frontSideId,
        backSideId: message.backSideId
      }

      const payload = elementalService.reportPayload(
        data,
        nconf.get('reportPath')
      )

      await elementalService.sendReport(payload, message.elementalClient)
    }
  } else {
    next()
  }
}

const provideReport = async (req, res, next) => {
  const docketId = req.body.docketId

  try {
    const isReportAvailable = await exists(
      `${nconf.get('dirs:pdfs')}${docketId}_front.pdf`
    )

    if (isReportAvailable) {
      // if doc available fetch data from db
      let document = await DocumentModel.find({
        docketId
      })
        .sort({
          $natural: -1
        })
        .limit(1)
        .lean()
      document = document[0]
      // parse au10tix data for document
      const parsedDocument = au10tixService.parseDocumentData(document)
      // provide report to elemental
      const data = _.pick(_.extend(parsedDocument, req.body), [
        'authentic',
        'docketId',
        'frontSideId',
        'backSideId'
      ])
      const payload = elementalService.reportPayload(
        data,
        nconf.get('reportPath')
      )
      // post report to elemental
      await elementalService.sendReport(payload, req.body.elementalClient)

      res.json({
        status: true,
        message: 'Report posted to elemental'
      })
    } else {
      // if report not available, move to start process and provide report
      next()
    }
  } catch (err) {
    if (!res.headersSent) {
      return res.status(500).json({
        status: false,
        message: 'something went wrong, try again.'
      })
    }
  }
}

module.exports = {
  validateRequest,
  handleFormUpload,
  handleForm,
  handleFilesinJSON,
  handleFilesError,
  provideReport
}
