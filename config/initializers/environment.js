const nconf = require('nconf')

// Set up configs
nconf.use('memory')
// Load environment variables
nconf.env()

const setup = () => {
  // pick related config
  const config = require('../environments/' + nconf.get('NODE_ENV') + '.json')

  for (const key in config) {
    nconf.set(key, config[key])
  }

  return true
}

module.exports = setup
