const del = require('del')
const path = require('path')
const appDir = path.join(__dirname, '../../')
const directories = require('./directories')

describe('Ensure directories test', () => {
  beforeEach(async () => {
    await del(['temp/test/dir-test'])
  })

  test('Throw error if directories provided are not an array of objects', async () => {
    await expect(
      directories({ dir: 'temp/test/not-an-array' })
    ).rejects.toThrow()
  })

  test('Throw error if directory path is not a string', async () => {
    await expect(
      directories([
        {
          dir: 1987,
          options: {
            mode: 0o2555
          }
        }
      ])
    ).rejects.toThrow()
  })

  test('Create multiple directories with paths in string', async () => {
    const data = await directories([
      {
        dir: 'temp/test/dir-test/'
      },
      {
        dir: 'temp/test/dir-test/uploads/'
      }
    ])

    const exprectedArr = [
      `${appDir}temp/test/dir-test`,
      `${appDir}temp/test/dir-test/uploads`
    ]

    expect(data).toEqual(expect.arrayContaining(exprectedArr))
  })
})
