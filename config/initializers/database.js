const mongoose = require('mongoose')
const fs = require('fs')
const util = require('util')
const path = require('path')
const ValidationError = require('../../util/validationError.util')

const readDir = util.promisify(fs.readdir)

const connect = async (dbUrl, options) => {
  if (typeof dbUrl !== 'string')
    throw new ValidationError(
      'MongoDb url is not a string',
      'ConnectDatabaseError'
    )

  const modelsPath = path.join(__dirname, '../../models')
  const models = await readDir(modelsPath)
  try {
    await mongoose.connect(dbUrl, options)

    console.log(`Au10tix MS → connected to mongoDB`)

    console.log(`Au10tix MS → Importing all database schemas`)

    models.forEach(model => {
      require(`${modelsPath}/${model}`)
    })

    console.log(`Au10tix MS → Imported all database schemas SUCCESSFULLY`)

    return true
  } catch (err) {
    throw err
  }
}

module.exports = connect
