const environment = require('./environment')

describe('Environment setup test', () => {
  test('Setup environment', () => {
    expect(environment()).toBe(true)
  })
})
