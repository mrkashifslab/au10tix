const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const connectDatabase = require('./database')
const ValidationError = require('../../util/validationError.util')

describe('Database init tests', () => {
  beforeAll(() => {
    require('./environment')()
  })

  afterEach(async () => {
    await mongoose.disconnect()
  })

  test('Throw error if db url passed is not a string', async () => {
    await expect(connectDatabase(1)).rejects.toBeInstanceOf(ValidationError)
  })

  test('Throw error if wrong mongodb url', async () => {
    await expect(connectDatabase('yada/yada/yada')).rejects.toThrow()
  })

  test('Throw error if connect to db in offline state', async () => {
    const mongoServer = new MongoMemoryServer()
    const mongoUri = await mongoServer.getConnectionString()
    await mongoServer.stop()
    await expect(
      connectDatabase(mongoUri, {
        useNewUrlParser: true
      })
    ).rejects.toThrow()
  })

  test('Connect to db and require all db models', async () => {
    const mongoServer = new MongoMemoryServer()
    const mongoUri = await mongoServer.getConnectionString()
    await expect(
      connectDatabase(mongoUri, {
        useNewUrlParser: true
      })
    ).resolves.toBe(true)
    await mongoServer.stop()
  })
})
