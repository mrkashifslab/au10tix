const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('../../apiDocs.json')
const routes = require('../../routes')

const start = async port => {
  const app = express()

  app.use(bodyParser.json())
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  )
  // Enabled cors to access the API's on the manual channel
  app.use(cors())

  app.use(
    morgan(
      '[:date[iso]] - :method - :url :status - :remote-addr - :remote-user - :response-time ms'
    )
  )

  app.use(
    '/api/docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, {
      customCss: '.swagger-ui .topbar { display: none }',
      customSiteTitle: `${swaggerDocument['info']['title']} - API docs`
    })
  )

  app.use('/', routes)

  try {
    await app.listen(port)

    console.log(`Au10tix MS → running on port ${port}`)
    return true
  } catch (err) {
    throw err
  }
}

module.exports = start
