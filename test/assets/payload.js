module.exports = {
  mockPayload: (port, fileName) => {
    const file = fileName || 'passport'
    return {
      docketId: 1896,
      reportProviderId: 3,
      documentTypeId: 32,
      autoTranscribe: true,
      frontSide: `http://localhost:${port}/file/${file}`,
      frontSideId: 4495,
      backSide: `http://localhost:${port}/file/${file}`,
      backSideId: 4495,
      monkeyQueue: '5caaa4272811133851344cee',
      elementalClient: {
        id: 4,
        name: 'Fazremit',
        apiKey: 'fc33d26e-00e3-4c35-82df-5e786efd4491',
        secretKey: 'ff5e9fc6-1094-400a-98cf-a3fb4ec8db12'
      }
    }
  },
  expOutput: {
    docketId: 1896,
    reportProviderId: 3,
    documentTypeId: 32,
    autoTranscribe: true,
    frontSideId: 4495,
    backSideId: 4495,
    monkeyQueue: '5caaa4272811133851344cee',
    elementalClient: {
      id: 4,
      name: 'Fazremit',
      apiKey: 'fc33d26e-00e3-4c35-82df-5e786efd4491',
      secretKey: 'ff5e9fc6-1094-400a-98cf-a3fb4ec8db12'
    }
  }
}
