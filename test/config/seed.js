const downloadFile = require('../../util/downloadFile.util')

;(async () => {
  try {
    await require('../../config/initializers/directories')([
      { dir: 'test/assets/temp' }
    ])
    await Promise.all([
      downloadFile(
        {
          url: 'https://via.placeholder.com/3000/09f.png/fff',
          path: 'test/assets/temp',
          fileName: '5mb.png'
        },
        { fileType: true, fileSize: true }
      ),
      downloadFile(
        {
          url: 'https://i.imgur.com/G4S4tCg.jpg',
          path: 'test/assets/temp',
          fileName: 'passport'
        },
        { fileType: true, fileSize: true }
      ),
      downloadFile(
        {
          url:
            'https://upload.wikimedia.org/wikipedia/commons/2/2d/Snake_River_%285mb%29.jpg',
          path: 'test/assets/temp',
          fileName: 'fileTooLarge'
        },
        { fileType: true, fileSize: true }
      )
    ])

    console.info('Seed data - complete')
  } catch (err) {
    console.error('error while seeding test', err)
  }
})()
