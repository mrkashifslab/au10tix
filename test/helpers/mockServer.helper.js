const fs = require('fs')
const multiparty = require('multiparty')
const { pipeline } = require('stream')
const express = require('express')
const app = express()
const au10tixPostRes = require('../assets/au10tixPost.res.json')
const au10tixGetRes = require('../assets/au10tixGet.res.json')

app.get('/file/:filename', async (req, res) => {
  try {
    await pipeline(
      fs.createReadStream(`test/assets/temp/${req.params.filename}`),
      res,
      err => {
        if (err) {
          console.log('caught error in streaming pipeline -', err)
        }
      }
    )
    return
  } catch (err) {
    console.log('Something went wrong in /file of mock server -', err.message)
  }
})

app.post('/au10tix/BeginProcessingRequest', (req, res) => {
  const form = new multiparty.Form()
  form.parse(req, (err, fields, files) => {
    if (err) throw err

    if (fields.requestData && files.frontSide && files.frontSide[0].size)
      res.json(au10tixPostRes['200'])
    else res.status(400).json(au10tixPostRes['400'])
  })
})

app.get('/au10tix/Results/:type', (req, res) => {
  const type = req.params.type

  if (type === 'error') {
    res.status(503)
    return
  }

  const data = au10tixGetRes['200'][type]

  data ? res.json(data) : res.status(200).send(null)
})

app.put('/monkey/api/v1/queue/:taskId/processed', (req, res) => {
  const taskId = req.params.taskId
  if (typeof taskId === 'string') {
    if (taskId === 'notaskinqueue') {
      res.status(404).json({
        status: false,
        message: 'task not found in queue'
      })

      return
    }
    res.json({
      status: true,
      message: 'task processing completed'
    })
  }
  res.status(400).json({
    status: false,
    message: 'task id should be a string'
  })
})

class MockServer {
  constructor(port) {
    this.port = port
  }

  async start() {
    if (this.server)
      throw new Error(
        `Server already running at port ${this.server.address().port}`
      )

    this.server = await app.listen(this.port)

    this.port = this.server.address().port

    this._warning = setInterval(
      () =>
        console.warn(
          `MockServer running on port ${this.port}, you forgot to stop it`
        ),
      1000
    )
  }

  async stop() {
    if (!this.server) throw new Error(`Server not running`)
    await this.server.close()
    clearInterval(this._warning)
  }
}

module.exports = MockServer
