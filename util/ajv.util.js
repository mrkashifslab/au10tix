const Ajv = require('ajv')
const ajv = new Ajv({ schemaId: 'auto' })

// validate json with its pre-defined schema
const jsonValidate = (data, schema) => {
  let validate = ajv.compile(schema)
  let valid = validate(data)

  if (!valid) throw new Error(ajv.errorsText(validate.errors))

  return true
}

module.exports = jsonValidate
