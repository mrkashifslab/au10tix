const MockServer = require('../test/helpers/mockServer.helper')
const downloadFileUtil = require('./downloadFile.util')
const expOutput = {
  extraInfo: {
    key: 'fileKey'
  },
  path: 'temp/test/uploads/'
}

describe('This function help download the documents', () => {
  const mockServer = new MockServer()
  let port
  beforeAll(async () => {
    require('../config/initializers/environment')()
    require('../config/initializers/directories')([
      {
        dir: 'temp/test/uploads'
      }
    ])

    await mockServer.start()
    port = mockServer.port
  })

  afterAll(async () => {
    await mockServer.stop()
  })

  test('It should download and store the document', async () => {
    const data = await downloadFileUtil({
      path: 'temp/test/uploads/',
      url: `http://localhost:${port}/file/passport`,
      extraInfo: {
        key: 'fileKey'
      }
    })

    expect(data).toMatchObject(expOutput)
  }, 10000)

  test('Throw error if destination path is not a string', async () => {
    await expect(
      downloadFileUtil({
        path: 1234,
        url: 'http://localhost:8889/passport',
        extraInfo: {
          key: 'fileKey'
        }
      })
    ).rejects.toThrow()
  })

  test('It should throw size exceeded Error', async () => {
    try {
      await downloadFileUtil(
        {
          path: 'temp/test/uploads/',
          url: `http://localhost:${port}/file/5mb.png`,
          extraInfo: {
            key: 'fileKey'
          }
        },
        { fileType: true }
      )
    } catch (err) {
      expect(err.name).toMatch('FileError')
    }
  })

  test('It should throw unsupported file type error', async () => {
    try {
      await downloadFileUtil(
        {
          path: 'temp/test/uploads/',
          url: `http://localhost:${port}/file/5mb.png`,
          extraInfo: {
            key: 'fileKey'
          }
        },
        { fileType: true }
      )
    } catch (err) {
      expect(err.name).toMatch('FileError')
    }
  })
})
