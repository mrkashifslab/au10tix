const env = process.env.NODE_ENV
const fs = require('fs')
const util = require('util')
const stream = require('stream')
const uuidv4 = require('uuid/v4')
const fileType = require('file-type')
const reqInstance =
  !env || env === 'test'
    ? require('./axios.util/req.axios')
    : require('./axios.util').reqInstance

const pipeline = util.promisify(stream.pipeline)

class FileError extends Error {
  constructor(message, url = '', type = '') {
    super(message)
    this.name = this.constructor.name
    this.url = url
    this.type = type
  }
}

// Download the file from a remote URL
const download = async ({ url, path, fileName, extraInfo }, config = {}) => {
  if (typeof path !== 'string') throw new Error('File path not defined')
  path = path[path.length - 1] === '/' ? path : path + '/'

  try {
    const res = await reqInstance.get(url, {
      responseType: 'stream'
    })

    const stream = await fileType.stream(res.data)
    const name = fileName || `${uuidv4()}.${stream.fileType.ext}`
    const fullPath = `${path || ''}${name}`
    const supportedFileTypes = ['jpg', 'tiff', 'pdf']
    const mb = 3.9
    let kb, by
    kb = by = 1024
    const maxFileSize = mb * kb * by
    const fileSize = res.headers['content-length']

    // validation
    if (!supportedFileTypes.includes(stream.fileType.ext) && !config.fileType) {
      throw new FileError(
        `Unsupported file type - ${stream.fileType.ext}`,
        url,
        'FileNotSupported'
      )
    }

    if (fileSize > maxFileSize && !config.fileSize) {
      throw new FileError(
        `File exceeds maximum file-size of 3.9MB. Size of file - ${fileSize /
          kb /
          by}MB`,
        url,
        'FileTooLarge'
      )
    }

    await pipeline(stream, fs.createWriteStream(fullPath))

    return {
      name,
      path,
      fullPath,
      extraInfo
    }
  } catch (err) {
    throw err
  }
}

module.exports = download
