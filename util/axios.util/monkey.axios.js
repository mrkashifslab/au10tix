const nconf = require('nconf')
const axios = require('axios')

const monkeyInstance = axios.create({
  baseURL: nconf.get('monkey:url')
})

module.exports = monkeyInstance
