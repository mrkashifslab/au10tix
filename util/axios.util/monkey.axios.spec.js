const env = require('../../config/environments/test.json')

describe('Monkey axios request monitor', () => {
  beforeAll(() => {
    require('../../config/initializers/environment')()
  })

  test('It should mock the network request for monkey', () => {
    const monkeyAxios = require('./monkey.axios')
    const MockAdapter = require('axios-mock-adapter')
    const mock = new MockAdapter(monkeyAxios)

    mock.onGet('').reply(200)
    monkeyAxios.get('').then(res => {
      expect(res.status).toBe(200)
      expect(res.config.baseURL).toBe(env.monkey.url)
    })
  })
})
