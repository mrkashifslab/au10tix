describe('Elemental axios request monitor', () => {
  beforeAll(() => {
    require('../../config/initializers/environment')()
  })

  const nconf = require('nconf')

  test('It should mock the GET request for elemental', () => {
    const elementalAxios = require('./elemental.axios')
    const MockAdapter = require('axios-mock-adapter')
    const mock = new MockAdapter(elementalAxios)

    mock.onGet('').reply(200)
    elementalAxios
      .get('', {
        elementalClient: {
          apiKey: 'yada-yada-yada',
          secretKey: 'yada-yada-yada'
        }
      })
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.config.baseURL).toBe(nconf.get('elemental:url'))
      })
      .catch(err => console.log(err))
  })

  test('It should mock the PUT request for elemental', () => {
    const elementalAxios = require('./elemental.axios')
    const MockAdapter = require('axios-mock-adapter')
    const mock = new MockAdapter(elementalAxios)

    mock.onPut('', { xyz: 'abc' }).reply(404)
    elementalAxios
      .put(
        '',
        {
          data: {
            xyz: 'abc'
          }
        },
        {
          elementalClient: {
            apiKey: 'yada-yada-yada',
            secretKey: 'yada-yada-yada'
          }
        }
      )
      .then(res => {
        expect(res.status).toBe(404)
        expect(res.config.baseURL).toBe(nconf.get('elemental:url'))
      })
      .catch(err => expect(err.response.status).toBe(404))
  })
})
