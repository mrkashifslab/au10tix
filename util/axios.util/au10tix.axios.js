const nconf = require('nconf')
const axios = require('axios')
const https = require('https')
const fs = require('fs')

const au10tixInstance = axios.create({
  baseURL: nconf.get('au10tix:baseURL'),
  headers: {
    Authorization: 'Certificate',
    'accept-encoding': 'gzip, deflate'
  },
  httpsAgent: new https.Agent({
    host: nconf.get('au10tix:host'),
    ca: fs.readFileSync(nconf.get('au10tix:certificatesDir') + '/ca'),
    cert: fs.readFileSync(nconf.get('au10tix:certificatesDir') + '/cert'),
    key: fs.readFileSync(nconf.get('au10tix:certificatesDir') + '/key'),
    passphrase: nconf.get('au10tix:passphrase')
  })
})

module.exports = au10tixInstance
