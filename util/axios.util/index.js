const au10tixInstance = require('./au10tix.axios')
const elementalInstance = require('./elemental.axios')
const monkeyInstance = require('./monkey.axios')
const reqInstance = require('./req.axios')

const instances = {
  au10tixInstance,
  monkeyInstance,
  elementalInstance,
  reqInstance
}

for (const instance in instances) {
  // Request Interceptor

  instances[instance].interceptors.request.use(
    function(config) {
      // Do something before request is sent
      console.log(
        'Au10tix MS → [message] - Outgoing Request',
        '[method] -',
        config.method,
        '[url] -',
        config.baseURL + config.url
      )
      return config
    },
    function(error) {
      // Do something with request error
      return Promise.reject(error)
    }
  )

  // Response Interceptor

  instances[instance].interceptors.response.use(
    function(response) {
      const { config, status } = response
      // Do something with response data
      console.log(
        'Au10tix MS → [message] - Incoming Response',
        '[method] -',
        config.method,
        '[url] -',
        config.url,
        '[status] -',
        status
      )
      return response
    },
    function(error) {
      // Do something with response error
      return Promise.reject(error)
    }
  )
}

module.exports = instances
