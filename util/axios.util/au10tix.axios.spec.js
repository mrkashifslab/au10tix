require('../../config/initializers/environment')()
const nconf = require('nconf')

describe('Au10tix axios request monitor', () => {
  test('It should mock the network request for Au10tix', () => {
    const axios = require('./au10tix.axios')
    const MockAdapter = require('axios-mock-adapter')
    const mock = new MockAdapter(axios)

    mock.onGet('https://jsonplaceholder.typicode.com/todos/1').reply(200)

    axios
      .get('https://jsonplaceholder.typicode.com/todos/1')
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.config.baseURL).toBe(nconf.get('au10tix:baseURL'))
      })
      .catch(err => console.log(err))
  })
})
