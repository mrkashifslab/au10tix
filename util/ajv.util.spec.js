const validationUtil = require('./ajv.util')

describe('Validation utility spec using AJV', () => {
  test('Check schema against correct payload', () => {
    expect(
      validationUtil(
        {
          frontSide: '/abc/test.js',
          docketId: 123
        },
        {
          properties: {
            docketId: {
              pattern: '^\\d+$'
            },
            frontSide: {
              format: 'uri-reference'
            }
          }
        }
      )
    ).toBe(true)
  })

  test('Check schema against incorrect value payload', () => {
    expect(() =>
      validationUtil(
        {
          frontSide: '/abc/test.js',
          docketId: '123abc'
        },
        {
          properties: {
            docketId: {
              pattern: '^\\d+$'
            },
            frontSide: {
              format: 'uri-reference'
            }
          }
        }
      )
    ).toThrow()
  })

  test('Check schema against missing required key in payload', () => {
    expect(() =>
      validationUtil(
        {
          frontSide: '/abc/test.js'
        },
        {
          properties: {
            docketId: {
              pattern: '^\\d+$'
            },
            frontSide: {
              format: 'uri-reference'
            }
          },
          required: ['frontSide', 'docketId']
        }
      )
    ).toThrow()
  })

  test('Check schema against additional key in payload', () => {
    expect(() =>
      validationUtil(
        {
          frontSide: '/abc/test.js',
          docketId: 123,
          additionalField: 'string'
        },
        {
          properties: {
            docketId: {
              pattern: '^\\d+$'
            },
            frontSide: {
              format: 'uri-reference'
            }
          },
          additionalProperties: false
        }
      )
    ).toThrow()
  })
})
