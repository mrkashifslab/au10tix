const ValidationError = require('./validationError.util')

describe('Validation error utility', () => {
  test('Validation error is an instance of Validation Error', () => {
    const message = 'this is an error'
    const validationError = new ValidationError(message)

    expect(validationError).toBeInstanceOf(ValidationError)
  })
})
