class ValidationError extends Error {
  constructor(message, name, info) {
    super(message)
    this.info = info
    this.name = name
  }
}

module.exports = ValidationError
