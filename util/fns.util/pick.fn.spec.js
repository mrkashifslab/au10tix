const { pick } = require('./')

const tObj = {
  name: 'john',
  age: 23,
  email: 'hello@john.com'
}

describe('Pick keys from object spec', () => {
  test('pick available keys from object', () => {
    expect(pick(tObj, ['email'])).toEqual({
      email: 'hello@john.com'
    })
  })

  test('pick unavailable keys from object', () => {
    const val = pick(tObj, ['address'])
    expect(val).toEqual({})
    expect(val).not.toEqual(tObj)
    expect(val).not.toBeNull()
  })
})
