module.exports = (target, ...sources) => {
  let source = []
  sources.forEach(src => {
    source = source.concat([src, Object.getPrototypeOf(src)])
  })
  return Object.assign(target, ...source)
}
