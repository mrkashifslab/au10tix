const pick = require('./pick.fn')
const extend = require('./extend.fn')

module.exports = {
  pick,
  extend
}
