const { extend } = require('./')

describe('Extend object spec', () => {
  test('replace keys in an object', () => {
    expect(
      extend(
        {
          name: 'john',
          age: 23
        },
        {
          name: 'doe'
        }
      )
    ).toEqual({
      name: 'doe',
      age: 23
    })
  })

  test('add keys to existing object', () => {
    expect(
      extend(
        {
          name: 'john'
        },
        {
          age: 23,
          email: 'hello@john.com'
        }
      )
    ).toEqual({
      name: 'john',
      age: 23,
      email: 'hello@john.com'
    })
  })
})
