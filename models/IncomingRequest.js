const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const IncomingRequestSchema = new mongoose.Schema(
  {},
  {
    strict: false,
    timestamps: true
  }
)

module.exports = mongoose.model('IncomingRequest', IncomingRequestSchema)
