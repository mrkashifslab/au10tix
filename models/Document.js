const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const documentSchema = new mongoose.Schema(
  {},
  {
    strict: false
  }
)

module.exports = mongoose.model('Document', documentSchema)
