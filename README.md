[![Bugs](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=bugs)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Code Smells](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=code_smells)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Coverage](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=coverage)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Duplicated Lines (%)](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=duplicated_lines_density)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Lines of Code](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=ncloc)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Maintainability Rating](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=sqale_rating)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Quality Gate Status](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=alert_status)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Reliability Rating](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=reliability_rating)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Security Rating](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=security_rating)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Technical Debt](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=sqale_index)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)
[![Vulnerabilities](https://sonar.alphafortress.com/api/project_badges/measure?project=microservice%3Aau10tix&metric=vulnerabilities)](https://sonar.alphafortress.com/dashboard?id=microservice%3Aau10tix)

# AU10TIX

It generates an authenticity report on the basis of any Govt. Identity Proof (Ex: Passport, Driving Licence, etc.)

## Get Started

Use the LTS version of [Node](https://nodejs.org/en/) to install packages.

1. npm install
2. copy the env files to config/environments
3. copy the certificates to certificates/"env"/
4. npm run dev

## Changelogs - 1.1.0

1. Enabled CORS

## Changelogs - 1.0.2

1.  Resolved auto transcription for back side of documents

## Changelogs - 1.0.1

1.  Changed authenticated kyc-block API's to unauthenticated `/message_broker` API's on kyc-block.

## Changelogs - 1.0.0

1.  AU10TIX api's integrated.

2.  Generates a detailed report basis on the Govt. ID.

3.  Error handled for AU10TIX api's.

4.  Report post back to KYC-Block.

5.  Report handling performed including False reports, success reports as well as File Size Error and File Type Error Reports.

6.  Kyc-Block logo attached to final report
