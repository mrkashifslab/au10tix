const MockServer = require('../test/helpers/mockServer.helper')
require('../config/initializers/environment')()
const { getFiles, taskComplete } = require('./monkey.service')
const { mockPayload, expOutput } = require('../test/assets/payload')

describe('It should change the payload and return the response', () => {
  const monkeyMockServer = new MockServer(8889)

  beforeAll(async () => {
    await require('../config/initializers/directories')([
      {
        dir: 'temp/test/pdfs/'
      },
      {
        dir: 'temp/test/uploads/'
      }
    ])
    await monkeyMockServer.start()
  })
  afterAll(async () => {
    await monkeyMockServer.stop()
  })

  test('It should replace the file paths in payload', async () => {
    const port = monkeyMockServer.port
    const filePath = await getFiles(mockPayload(port), 'temp/test/uploads/')

    expect(filePath).toMatchObject(expOutput)
    expect(filePath.frontSide).toEqual(expect.any(String))
    expect(filePath.backSide).toEqual(expect.any(String))
  })

  test('It should throw error due to missing filePath', async () => {
    const port = monkeyMockServer.port

    await expect(getFiles(mockPayload(port))).rejects.toThrow()
  })

  test('Throw error if task not found', async () => {
    try {
      await taskComplete('notaskinqueue')
    } catch (err) {
      expect(err.info).toMatchObject({
        status: false,
        message: 'task not found in queue'
      })
      expect(err.status).toBe(404)
    }
  })

  test('Update task successfully', async () => {
    const { taskComplete } = require('./monkey.service')
    const response = await taskComplete('5c0fa894dbef4170b845e540')

    expect(response.data).toMatchObject({
      status: true,
      message: 'task processing completed'
    })
    expect(response.status).toBe(200)
  })

  test('It should get file size too large error', async () => {
    const port = monkeyMockServer.port
    try {
      await getFiles(mockPayload(port, 'fileTooLarge'), 'temp/test/uploads/')
    } catch (error) {
      expect(error.name).toBe('FileError')
      expect(error.status).toBe(406)
    }
  })
})
