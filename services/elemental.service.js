const env = process.env.NODE_ENV
const nconf = require('nconf')
const _ = require('../util/fns.util')
const elementalInstance =
  !env || env === 'test'
    ? require('../util/axios.util/elemental.axios')
    : require('../util/axios.util').elementalInstance

class ElementalServiceError extends Error {
  constructor({ message, from, info }) {
    super(message)
    this.info = info
    this.name = 'ElementalServiceError'
    this.from = from
  }
}

const transcribeDoc = parsedDocument => {
  const transcription = {}
  let { documentData, error } = parsedDocument

  if (error) return { note: error.message }

  const reqKeys = [
    'Address',
    'Birthplace',
    'Date Of Birth',
    'Date Of Expiry',
    'Date Of Issue',
    'Document Number',
    'First Name',
    'Middle Name',
    'Last Name',
    'Gender',
    'Nationality'
  ]

  documentData = _.pick(documentData, reqKeys)

  for (const key in documentData) {
    let documentDataKey = key.toLowerCase()
    let transcriptionKey = documentDataKey
    let value = documentData[key]

    switch (documentDataKey) {
      case 'date of birth':
      case 'date of issue':
      case 'date of expiry':
        value = value.split('T')[0]
        break

      default:
        break
    }

    transcription[transcriptionKey] = value
  }

  if (transcription.hasOwnProperty('optional data')) {
    delete transcription['optional data']
  }

  if (transcription.hasOwnProperty('mrz')) {
    delete transcription.mrz
  }

  return transcription
}

const sendTranscription = async ({ documentId, document }, elementalClient) => {
  try {
    return await elementalInstance.put(
      `message_broker/documents/${documentId}/auto_transcribable_update`,
      {
        document
      },
      {
        headers: {
          elementalClient
        }
      }
    )
  } catch (err) {
    throw new ElementalServiceError({
      message: err.message,
      from: 'Send Transcription',
      info: err.response ? err.response.data : {}
    })
  }
}

const reportPayload = (
  { authentic, docketId, frontSideId, backSideId },
  reportPath
) => {
  const doc = {
    remote_url: `${reportPath}/${docketId + '_front.pdf'}`,
    document_name: docketId + '_front.pdf',
    file_type: 'pdf',
    docket_id: docketId,
    report_document_ids: [frontSideId]
  }

  if (backSideId) doc.report_document_ids.push(backSideId)

  doc.document_type_id = nconf.get('elemental:documentTypeId')
  doc.document_type_name = nconf.get('elemental:documentTypeName')
  doc.report_provider_id = nconf.get('elemental:reportProviderId')
  doc.document_metadata = {
    status: authentic
  }
  return doc
}

const sendReport = async (document, elementalClient) => {
  try {
    return await elementalInstance.post(
      `message_broker/reports`,
      {
        document
      },
      {
        headers: {
          elementalClient
        }
      }
    )
  } catch (err) {
    throw new ElementalServiceError({
      message: err.message,
      from: 'Send Report',
      info: err.response ? err.response.data : {}
    })
  }
}

const finishTranscription = async (documentId, elementalClient) => {
  try {
    await elementalInstance.put(
      `message_broker/documents/${documentId}/transcribe`,
      {},
      {
        headers: {
          elementalClient
        }
      }
    )
  } catch (err) {
    throw new ElementalServiceError({
      message: err.message,
      from: 'Finish Transcribe',
      info: err.response ? err.response.data : {}
    })
  }
}

module.exports = {
  transcribeDoc,
  sendTranscription,
  reportPayload,
  sendReport,
  finishTranscription
}
