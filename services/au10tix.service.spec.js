const expOutputAu10tixPost = require('../test/assets/au10tixPost.res.json')
const expOutputAu10tixGet = require('../test/assets/au10tixGet.res.json')
const au10tixParseDocument = require('../test/assets/au10tixParseDocument.json')

describe('Au10tix Service Payload verification', () => {
  require('../config/initializers/environment')()
  const au10tixService = require('./au10tix.service')
  const MockServer = require('../test/helpers/mockServer.helper')
  const mockServer = new MockServer(8888)

  beforeAll(async () => {
    await mockServer.start().catch
  })

  afterAll(async () => {
    await mockServer.stop()
  })

  test('It successfully sends the document to au10tix', async () => {
    try {
      const documentData = await au10tixService.getDocumentData('Ok')
      expect(documentData.data.CompletionStatus).toBe('Ok')
    } catch (error) {
      console.log(error)
    }
  })

  test('It rejects the request sends', async () => {
    try {
      const documentData = await au10tixService.getDocumentData(
        'RequestRejected'
      )
      expect(documentData.data.CompletionStatus).toBe('RequestRejected')
    } catch (error) {
      console.log(error)
    }
  })

  test('An Unrecognizable Document in payload', async () => {
    try {
      const documentData = await au10tixService.getDocumentData(
        'UnrecognizableDocument'
      )
      expect(documentData.data.CompletionStatus).toBe('UnrecognizableDocument')
    } catch (error) {
      console.log(error)
    }
  })

  test('Pages in the payload is treated as separate document', async () => {
    const documentData = await au10tixService.getDocumentData(
      'PagesTreatedAsSeparateDocuments'
    )
    expect(documentData.data.CompletionStatus).toBe(
      'PagesTreatedAsSeparateDocuments'
    )
  })

  test('It should get bad request due to missing frontSide in Send Document', async () => {
    await expect(
      au10tixService.sendDocuments({
        backSide: 'test/assets/temp/passport'
      })
    ).rejects.toThrow()
  })

  test('It should get executed successfully after sending documents', async () => {
    const documentData = await au10tixService.sendDocuments({
      backSide: 'test/assets/temp/passport',
      frontSide: 'test/assets/temp/passport'
    })

    expect(documentData.data).toMatchObject(expOutputAu10tixPost['200'])
  })

  test('It should get Unsupported document or no data found', () => {
    const documentData = au10tixService.parseDocumentData({
      _id: '5cdbf8cfd19544c71d1ec969',
      docketId: 18926,
      incomingReq: '5cdbf8c1d19544c71d1ec968'
    })
    expect(documentData).toMatchObject(
      expOutputAu10tixGet['processDocumentError']
    )
  })

  test('Parse document with payload as OK', () => {
    const documentData = au10tixService.parseDocumentData(
      expOutputAu10tixGet['200']['Ok']
    )

    expect(documentData).toMatchObject(au10tixParseDocument['Ok'])
  })

  test('Parse document with payload as PagesTreatedAsSeparateDocuments', () => {
    const documentData = au10tixService.parseDocumentData(
      expOutputAu10tixGet['200']['PagesTreatedAsSeparateDocuments']
    )
    // console.log(documentData)
    expect(documentData).toMatchObject(
      au10tixParseDocument['PagesTreatedAsSeparateDocuments']
    )
  })

  test('Parse document with payload as Unrecongnizable data', () => {
    const documentData = au10tixService.parseDocumentData(
      expOutputAu10tixGet['200']['UnrecognizableDocument']
    )
    expect(documentData).toMatchObject(
      au10tixParseDocument['UnrecognizableDocument']
    )
  })

  test('Parse document with payload as Rejected Request', () => {
    const documentData = au10tixService.parseDocumentData(
      expOutputAu10tixGet['200']['RequestRejected']
    )

    expect(documentData).toMatchObject(au10tixParseDocument['RequestRejected'])
  })
})
