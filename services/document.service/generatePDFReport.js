const pdf = require('pdfjs')
const fs = require('fs')
const hummus = require('hummus')
const { pipeline } = require('stream')
const path = require('path')

const fonts = {
  OpenSansLight: new pdf.Font(
    fs.readFileSync(
      path.join(__dirname, '../../assets/fonts/', 'OpenSans-Light.ttf')
    )
  ),
  OpenSansBold: new pdf.Font(
    fs.readFileSync(
      path.join(__dirname, '../../assets/fonts/', 'OpenSans-Bold.ttf')
    )
  )
}

const genPdf = (data, imagesLocation, pdfOutputDir) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      pdfOutputDir + data.docketId + '.json',
      JSON.stringify(data),
      'utf8',
      function(err) {
        if (err) {
          console.log(
            'Au10tix MS → An error occured while writing JSON Object to file'
          )
          return err
        }
        console.log('Au10tix MS → JSON file has been saved. -', data.docketId)
      }
    )

    let {
      authentic,
      docketId,
      documentData,
      documentQuality,
      documentTests,
      error
    } = data

    const doc = new pdf.Document({
      font: fonts.OpenSansLight
    })

    const header = doc
      .header()
      .table({
        widths: [null]
      })
      .row()

    header
      .cell('')
      .image(new pdf.Image(fs.readFileSync('assets/images/logo.jpg')), {
        height: 2 * pdf.cm,
        align: 'center'
      })

    doc.footer().pageNumber(
      function(curr, total) {
        return curr + ' / ' + total
      },
      {
        textAlign: 'center'
      }
    )

    doc.cell({
      paddingBottom: 0.1 * pdf.cm
    })

    if (error instanceof Object) {
      doc
        .table({
          widths: [null]
        })
        .row()
        .cell(authentic ? 'OK' : 'NOT OK', {
          fontSize: 16,
          font: fonts.OpenSansBold,
          backgroundColor: authentic ? 0xa1d081 : 0xfc0204,
          color: 0xffffff,
          padding: 5
        })
        .text('Docket ID: ' + docketId, {
          fontSize: 10,
          font: fonts.OpenSansLight
        })

      const table = doc.table({
        widths: [null, null]
      })

      table
        .header({
          fontSize: 14,
          font: fonts.OpenSansBold,
          borderBottomWidth: 1.5
        })
        .cell('ERROR')

      let tr = table.row()
      tr.cell('Message :')
      tr.cell(error.message, {
        textAlign: 'right'
      })
    } else {
      doc
        .table({
          widths: [null]
        })
        .row()
        .cell(authentic ? 'OK' : 'NOT OK', {
          fontSize: 16,
          font: fonts.OpenSansBold,
          backgroundColor: authentic ? 0xa1d081 : 0xfc0204,
          color: 0xffffff,
          padding: 5
        })
        .text('Docket ID: ' + docketId, {
          fontSize: 10,
          font: fonts.OpenSansLight
        })

      const table1 = doc.table({
        widths: [null, null]
      })

      table1
        .header({
          fontSize: 14,
          font: fonts.OpenSansBold,
          borderBottomWidth: 1.5
        })
        .cell('Document Data')

      for (let key in documentData) {
        if (documentData[key]) {
          let tr = table1.row()
          tr.cell(key + ':')
          tr.cell(documentData[key], {
            textAlign: 'right'
          })
        }
      }

      doc.cell({
        paddingBottom: 0.1 * pdf.cm
      })

      const table2 = doc.table({
        widths: [null, null]
      })
      table2
        .header({
          fontSize: 14,
          font: fonts.OpenSansBold,
          borderBottomWidth: 1.5
        })
        .cell('Quality Information')

      for (const key in documentQuality) {
        let tr = table2.row()
        tr.cell(key + ':')
        tr.cell(documentQuality[key], {
          textAlign: 'right',
          color: documentQuality[key] === 'Acceptable' ? 0xa1d081 : '0xFC0204'
        })
      }

      doc.cell({
        paddingBottom: 0.1 * pdf.cm
      })

      const table3 = doc.table({
        widths: [null, null]
      })

      table3
        .header({
          fontSize: 14,
          font: fonts.OpenSansBold,
          borderBottomWidth: 1.5
        })
        .cell('Forgery Test results')

      for (const key in documentTests) {
        const tr = table3.row()
        tr.cell(key + ':')
        tr.cell(documentTests[key], {
          textAlign: 'right',
          color: documentTests[key] === 'Authentic' ? 0xa1d081 : 0xfc0204
        })
      }
    }

    const docName = data.docketId + '_front.pdf'
    const docNameBack = data.docketId + '_back.pdf'

    doc.end().catch(err => {
      console.log('Au10tix MS → pdf error', err)
      reject(err)
    })

    const outputStream = fs.createWriteStream(pdfOutputDir + docName)

    if (typeof pdfOutputDir !== 'string') outputStream.destroy()

    pipeline(doc, outputStream, err => {
      if (err) {
        console.log(
          'Au10tix MS → document',
          docketId,
          'pdf pipeline err -',
          err
        )
        throw err
      }

      if (imagesLocation) {
        const pdfWriter = hummus.createWriterToModify(pdfOutputDir + docName)

        Object.keys(imagesLocation).forEach((image, i) => {
          if (fs.existsSync(imagesLocation[image])) {
            try {
              const page = pdfWriter.createPage(0, 0, 595, 842)
              const cxt = pdfWriter.startPageContentContext(page)
              cxt.drawImage(0, 0, imagesLocation[image], {
                transformation: {
                  width: 595,
                  height: 842,
                  proportional: true,
                  fit: 'always'
                }
              })
              pdfWriter.writePage(page)
            } catch (err) {
              console.log('Au10tix MS → adding img err -', err)
            }
            fs.unlinkSync(imagesLocation[image])
          }
        })
        pdfWriter.end()
      }
      console.log('Au10tix MS → saved', docName)
      if (
        imagesLocation.hasOwnProperty('frontSide') &&
        imagesLocation.hasOwnProperty('backSide')
      )
        fs.copyFileSync(pdfOutputDir + docName, pdfOutputDir + docNameBack)

      resolve({
        front: docName,
        back: docNameBack,
        authentic,
        documentData
      })
    })
  })
}

module.exports = genPdf
