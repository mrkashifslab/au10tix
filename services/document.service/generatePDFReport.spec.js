const MockServer = require('../../test/helpers/mockServer.helper')
const generatePdfReport = require('./generatePDFReport')
const downloadFileUtil = require('../../util/downloadFile.util')
const pdfJson = require('../../test/assets/pdfJson.json')
const fs = require('fs-extra')

describe('It should generate the PDF report', () => {
  const mockServer = new MockServer()
  let port

  beforeAll(async () => {
    await require('../../config/initializers/environment')()
    await require('../../config/initializers/directories')([
      {
        dir: 'temp/test/pdfs/'
      },
      {
        dir: 'temp/test/uploads/'
      }
    ])

    await mockServer.start()
    port = mockServer.port
  })

  afterAll(async () => {
    await mockServer.stop()
  })

  test('It should create the PDF error report data', async () => {
    const downloadData = await downloadFileUtil({
      path: 'temp/test/uploads/',
      url: `http://localhost:${port}/file/passport`,
      extraInfo: {
        key: 'fileKey'
      }
    })

    const pdfData = await generatePdfReport(
      {
        authentic: false,
        docketId: 1896,
        documentData: null,
        documentQuality: null,
        documentTests: null,
        error: {
          message:
            'Rejected request, reason - IdentityDocumentRequestAllPagesRejected'
        }
      },
      {
        frontSide: downloadData.fullPath
      },
      'temp/test/pdfs/'
    )

    const exists = await fs.pathExists(`temp/test/pdfs/${pdfData.front}`)

    expect(exists).toBe(true)
  })

  test('It should create the PDF with complete report data', async () => {
    const downloadData = await downloadFileUtil({
      path: 'temp/test/uploads/',
      url: `http://localhost:${port}/file/passport`,
      extraInfo: {
        key: 'fileKey'
      }
    })

    const pdfData = await generatePdfReport(
      pdfJson,
      {
        frontSide: downloadData.fullPath
      },
      'temp/test/pdfs/'
    )

    const exists = await fs.pathExists(`temp/test/pdfs/${pdfData.front}`)

    expect(exists).toBe(true)
  })

  test('File path can only be string', async () => {
    const downloadData = await downloadFileUtil({
      path: 'temp/test/uploads/',
      url: `http://localhost:${port}/file/passport`,
      extraInfo: {
        key: 'fileKey'
      }
    })

    await expect(() =>
      generatePdfReport(
        {
          authentic: false,
          docketId: 1896,
          documentData: null,
          documentQuality: null,
          documentTests: null,
          error: {
            message:
              'Rejected request, reason - IdentityDocumentRequestAllPagesRejected'
          }
        },
        {
          frontSide: downloadData.fullPath
        },
        12345
      )
    ).rejects
  })
})
