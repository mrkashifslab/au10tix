const mongoose = require('mongoose')
const IncomingRequestModel = mongoose.model('IncomingRequest')
const DocumentModel = mongoose.model('Document')
const _ = require('../../util/fns.util')
const generatePdfReport = require('./generatePDFReport')
const au10tixService = require('../au10tix.service')
const elementalService = require('../elemental.service')
const monkeyService = require('../monkey.service')

const processDoc = async (requestPayload, config = {}) => {
  // default configuration to process documents
  const defaultConfig = {
    uploadPath: 'temp/dev/uploads/',
    pdfPath: 'pdfDocs/dev/',
    reportPath: null,
    sendTranscription: false,
    sendReport: true
  }

  // override default config with provided config
  config = _.extend(defaultConfig, config)

  try {
    const fileKeys = ['frontSide', 'backSide', 'photo']

    // get fileKeys from incoming request
    const documentFiles = _.pick(requestPayload, fileKeys)

    // pass files & send to au10tix & start processing files
    const documentsRequest = await au10tixService.sendDocuments(documentFiles)

    // save incoming document processing request from au10tix
    await IncomingRequestModel.findOneAndUpdate(
      {
        _id: requestPayload._id
      },
      documentsRequest.data,
      {
        new: true
      }
    )

    // wait 10 seconds till the file gets processed at au10tix
    await new Promise(resolve => setTimeout(resolve, 10000))

    // get document data from au10tix
    const documentData = await au10tixService.getDocumentData(
      documentsRequest.data.RequestId,
      1
    )

    // insert recieved document in database
    let document = await DocumentModel({
      ...documentData.data,
      docketId: requestPayload.docketId,
      incomingReq: requestPayload._id.toString()
    }).save()

    document = document._doc

    // update incoming request with recieved document data id
    await IncomingRequestModel.findOneAndUpdate(
      {
        _id: requestPayload._id
      },
      {
        document: document._id.toString()
      }
    )

    // parse recieved document from au10tix
    const parsedDocument = au10tixService.parseDocumentData(document)

    // genereate pdf report
    await generatePdfReport(parsedDocument, documentFiles, config.pdfPath)

    // provide transcription to elemental if conditions meet
    if (config.sendTranscription && requestPayload.elementalClient) {
      // get transcription for elemental from parsed document
      const transcription = elementalService.transcribeDoc(parsedDocument)

      // post transcription to elemental

      await elementalService.sendTranscription(
        {
          documentId: requestPayload.frontSideId,
          document: {
            document_metadata: transcription
          }
        },
        requestPayload.elementalClient
      )
      // post backside transcription to elemental if payload have backside of identity
      if (documentFiles.backSide) {
        await elementalService.sendTranscription(
          {
            documentId: requestPayload.backSideId,
            document: {
              document_metadata: transcription
            }
          },
          requestPayload.elementalClient
        )
      }

      await elementalService.finishTranscription(
        requestPayload.frontSideId,
        requestPayload.elementalClient
      )

      if (documentFiles.backSide) {
        await elementalService.finishTranscription(
          requestPayload.backSideId,
          requestPayload.elementalClient
        )
      }
    }

    // provide report to elemental if conditions meet
    // if (config.sendReport && requestPayload.elementalClient) {
    // const data = _.pick(_.extend(parsedDocument, requestPayload), [
    //   'authentic',
    //   'docketId',
    //   'frontSideId',
    //   'backSideId'
    // ])
    // const payload = elementalService.reportPayload(data, config.reportPath)
    // post report to elemental
    // await elementalService.sendReport(payload, requestPayload.elementalClient)
    // }

    // post task completion to monkey
    await monkeyService.taskComplete(requestPayload.monkeyQueue)

    return true
  } catch (err) {
    console.log('processDoc err -', err)

    throw err
  }
}

module.exports = processDoc
