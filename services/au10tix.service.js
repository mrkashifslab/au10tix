const env = process.env.NODE_ENV
const FormData = require('form-data')
const fs = require('fs')
const au10tixInstance =
  !env || env === 'test'
    ? require('../util/axios.util/au10tix.axios')
    : require('../util/axios.util').au10tixInstance

class Au10tixServiceError extends Error {
  constructor({ message, from, info }) {
    super(message)
    this.info = info
    this.name = 'Au10tixServiceError'
    this.from = from
  }
}

// Sends the document to AU10TIX
const sendDocuments = async (files, attempts = 1) => {
  const form = new FormData()

  let double = {
    identityDocumentProcessingRequest: {
      documentPages: [
        {
          documentImages: [
            {
              imageUri: 'frontSide'
            }
          ]
        },
        {
          documentImages: [
            {
              imageUri: 'backSide'
            }
          ]
        }
      ]
    }
  }

  let noPhotoKeysCount = 0

  for (const key in files) {
    const dKey = key === 'photo' ? 'photoForFaceComparison' : key

    form.append(dKey, fs.createReadStream(files[key]))
    if (key !== 'photo') noPhotoKeysCount++
  }

  if (noPhotoKeysCount > 1) {
    form.append('requestData', JSON.stringify(double))
  } else {
    form.append(
      'requestData',
      JSON.stringify({
        identityDocumentProcessingRequest: {
          documentPages: [
            {
              documentImages: [
                {
                  imageUri: 'frontSide',
                  single: 'yeah!'
                }
              ]
            }
          ]
        }
      })
    )
  }

  try {
    return await au10tixInstance.post('/BeginProcessingRequest', form, {
      headers: form.getHeaders()
    })
  } catch (err) {
    if (attempts <= 2) return sendDocuments(files, attempts + 1)

    throw new Au10tixServiceError({
      message: err.message,
      from: 'Send Documents',
      info: err.response ? err.response.data : {}
    })
  }
}

//  Return result provided by AU10TIX
const getDocumentData = async (id, attempts = 1) => {
  try {
    const res = await au10tixInstance.get('/Results/' + id)
    if (!res.data) throw new Error('Au10tix - No data found')
    return res
  } catch (err) {
    if (attempts <= 3) {
      console.log(`Au10tix MS → Get Document Data Attempt ${attempts}`)

      await new Promise(resolve => setTimeout(resolve, 10000))
      return getDocumentData(id, attempts + 1)
    }

    throw new Au10tixServiceError({
      message: err.message,
      from: 'Get Documents',
      info: err.response ? err.response.data : {}
    })
  }
}

const parseDocumentData = payload => {
  let authentic = false
  let documentData
  let documentQuality
  let documentTests
  let error

  if (!payload.CompletionStatus) {
    error = {
      message: 'Unsupported document or no data found'
    }
  }

  switch (payload.CompletionStatus) {
    case 'RequestRejected':
      console.info(
        'Au10tix rejected request for incoming request -',
        payload.incomingReq,
        'reason',
        payload.RequestRejectionInfo
      )

      error = {
        message: `Rejected request, reason - ${
          payload.RequestRejectionInfo.ErrorMessage
        }`
      }
      break

    case 'UnrecognizableDocument':
      console.info(
        'Could not recognize the document for incoming request -',
        payload.incomingReq
      )

      error = {
        message: 'Could not recognize the documents provided'
      }
      break

    case 'Ok':
      authentic = payload.DocumentAuthenticity === 'Authentic'
      documentData = payload.ProcessingResult.DocumentData2
      documentQuality =
        payload.ProcessingResult.PageProcessingResults[0].ProcessedImages[0]
          .Quality
      documentTests = payload.ProcessingResult.ForgeryTests
      break

    case 'PagesTreatedAsSeparateDocuments':
      authentic =
        payload.PageAsSeparateDocumentProcessingReports[0]
          .DocumentAuthenticity === 'Authentic'
      documentData =
        payload.PageAsSeparateDocumentProcessingReports[0].ProcessingResult
          .DocumentData2
      documentQuality =
        payload.PageAsSeparateDocumentProcessingReports[0].ProcessingResult
          .PageProcessingResults[0].ProcessedImages[0].Quality
      documentTests =
        payload.PageAsSeparateDocumentProcessingReports[0].ProcessingResult
          .ForgeryTests
      break

    default:
      break
  }

  let result = {
    authentic,
    docketId: payload.docketId,
    documentData: null,
    documentQuality: null,
    documentTests: null,
    error
  }

  if (documentData) {
    result.documentData = {}

    for (let key in documentData) {
      if (documentData[key]) {
        result.documentData[key.replace(/([A-Z])/g, ' $1').trim()] =
          documentData[key].Value || ''
      }
    }
  }

  if (documentQuality) {
    result.documentQuality = {}

    for (const key in documentQuality) {
      result.documentQuality[
        key
          .replace('IsAcceptable', '')
          .replace(/([A-Z])/g, ' $1')
          .trim()
      ] = documentQuality[key] ? 'Acceptable' : 'Not Acceptable'
    }
  }

  if (documentTests) {
    result.documentTests = {}

    if (documentTests instanceof Array) {
      documentTests.forEach(test => {
        result.documentTests[test.ForgerySubtype + ' - ' + test.ForgeryType] =
          test.TestResult
      })
    }

    if (documentTests instanceof Object) {
      for (const key in documentTests) {
        result.documentTests[
          documentTests[key].ForgerySubtype +
            ' - ' +
            documentTests[key].ForgeryType
        ] = documentTests[key].TestResult
      }
    }
  }

  return result
}

module.exports = {
  sendDocuments,
  getDocumentData,
  parseDocumentData
}
