const nconf = require('nconf')
const env = nconf.get('NODE_ENV')
const _ = require('../util/fns.util')
const downloadFileUtil = require('../util/downloadFile.util')

const monkeyInstance =
  !env || env === 'test'
    ? require('../util/axios.util/monkey.axios')
    : require('../util/axios.util').monkeyInstance

class MonkeyServiceError extends Error {
  constructor({ message, from, info, status }) {
    super(message)
    this.info = info
    this.status = status
    this.name = 'MonkeyServiceError'
    this.from = from
  }
}

// Download the file from url's provided in payload
const getFiles = async (message, path) => {
  const files = ['frontSide', 'backSide', 'photo']
  const pickedMsg = _.pick(message, files)
  const promises = []

  for (const fileKey in pickedMsg) {
    promises.push(
      downloadFileUtil({
        path,
        url: pickedMsg[fileKey],
        extraInfo: {
          key: fileKey
        }
      })
    )
  }

  try {
    const files = await Promise.all(promises)

    // Change file url with uploaded path in payload
    files.forEach(file => (message[file.extraInfo.key] = file.fullPath))

    return message
  } catch (err) {
    throw err
  }
}

// Acknowledge Monkey about the task
const taskComplete = async queueId => {
  try {
    return await monkeyInstance.put(`/api/v1/queue/${queueId}/processed`)
  } catch (err) {
    throw new MonkeyServiceError({
      message: err.message,
      from: 'Task Complete',
      info: err.response ? err.response.data : null,
      status: err.response ? err.response.status : null
    })
  }
}

module.exports = {
  getFiles,
  taskComplete
}
